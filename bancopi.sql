-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 05-Dez-2019 às 21:15
-- Versão do servidor: 8.0.18
-- versão do PHP: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `bancopi`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

DROP TABLE IF EXISTS `noticias`;
CREATE TABLE IF NOT EXISTS `noticias` (
  `id_noticia` int(50) NOT NULL AUTO_INCREMENT,
  `data_noticia` varchar(30) NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `lado` varchar(30) NOT NULL,
  `titulo_principal` varchar(150) NOT NULL,
  `titulo_div` varchar(150) NOT NULL,
  `titulo_banner` varchar(150) NOT NULL,
  `resumo_principal` varchar(200) NOT NULL,
  `resumo_div` varchar(200) NOT NULL,
  `resumo_banner` varchar(200) NOT NULL,
  `texto_div` varchar(150) NOT NULL,
  `texto_noticia` text NOT NULL,
  `img_pequena` varchar(30) NOT NULL,
  `img_grande` varchar(30) NOT NULL,
  PRIMARY KEY (`id_noticia`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `noticias`
--

INSERT INTO `noticias` (`id_noticia`, `data_noticia`, `tipo`, `lado`, `titulo_principal`, `titulo_div`, `titulo_banner`, `resumo_principal`, `resumo_div`, `resumo_banner`, `texto_div`, `texto_noticia`, `img_pequena`, `img_grande`) VALUES
(1, '05/06/2019', 'Economia', 'Direito', 'Boicote à carne não reduziria preço e só afetaria açougue, dizem analistas...', 'Boicote à carne não reduziria preço e só afetaria açougue, dizem analistas...', 'Boicote à carne não reduziria preço e só afetaria açougue, dizem analistas...', 'Após aumento no preço, grupos pedem boicote à compra de carne bovina', 'Após aumento no preço, grupos pedem boicote à compra de carne bovina', 'Após aumento no preço, grupos pedem boicote à compra de carne bovina', 'Em resposta à disparada nos preços da carne bovina no país, internautas começaram a compartilhar a ideia de um boicote ao produto. Segundo postagens..', 'Em resposta à disparada nos preços da carne bovina no país, internautas começaram a compartilhar a ideia de um boicote ao produto. Segundo postagens nas redes sociais, como a carne de vaca é um alimento perecível, se os consumidores pararem de comprá-la e optarem por frango ou porco, os açougues não terão outra opção que não seja baixar o preço. Mas não funciona bem assim. De acordo com economistas ouvidos pelo UOL, uma tentativa de boicote só teria efeitos pontuais, não conseguiria reduzir o preço geral e prejudicaria apenas quem está na ponta da cadeia produtiva, os açougues e supermercados.... ', 'CarneP.jpg', 'CarneG.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias1`
--

DROP TABLE IF EXISTS `noticias1`;
CREATE TABLE IF NOT EXISTS `noticias1` (
  `id_noticia` int(50) NOT NULL AUTO_INCREMENT,
  `data_noticia` varchar(40) NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `lado` varchar(30) NOT NULL,
  `titulo_principal` varchar(150) NOT NULL,
  `resumo_principal` varchar(100) NOT NULL,
  `texto_noticia` text NOT NULL,
  `imagem` varchar(300) NOT NULL,
  PRIMARY KEY (`id_noticia`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `noticias1`
--

INSERT INTO `noticias1` (`id_noticia`, `data_noticia`, `tipo`, `lado`, `titulo_principal`, `resumo_principal`, `texto_noticia`, `imagem`) VALUES
(1, '2019-12-05 19:51:52', '', '', 'Boicote à carne não reduziria preço e só afetaria açougue, dizem analistas...', 'Após aumento no preço, grupos pedem boicote à compra de carne bovinaa222w', 'Em resposta à disparada nos preços da carne bovina no país, internautas começaram a compartilhar a ideia de um boicote ao produto. Segundo postagens nas redes sociais, como a carne de vaca é um alimento perecível, se os consumidores pararem de comprá-la e optarem por frango ou porco, os açougues não terão outra opção que não seja baixar o preço. Mas não funciona bem assim. De acordo com economistas ouvidos pelo UOL, uma tentativa de boicote só teria efeitos pontuais, não conseguiria reduzir o preço geral e prejudicaria apenas quem está na ponta da cadeia produtiva, os açougues e supermercados.... ', '0e080857e96278e6dba76ac029faf291.jpg'),
(2, '2019-12-05 19:53:14', '', '', 'Boicote à carne não reduziria preço e só afetaria açougue, dizem analistas...', 'Após aumento no preço, grupos pedem boicote à compra de carne bovina', 'Em resposta à disparada nos preços da carne bovina no país, internautas começaram a compartilhar a ideia de um boicote ao produto. Segundo postagens nas redes sociais, como a carne de vaca é um alimento perecível, se os consumidores pararem de comprá-la e optarem por frango ou porco, os açougues não terão outra opção que não seja baixar o preço. Mas não funciona bem assim. De acordo com economistas ouvidos pelo UOL, uma tentativa de boicote só teria efeitos pontuais, não conseguiria reduzir o preço geral e prejudicaria apenas quem está na ponta da cadeia produtiva, os açougues e supermercados.... ', 'bb181e83b9ac6be1b28b2a2b26dcd73e.jpg'),
(3, '2019-12-05 19:54:08', '', '', 'Boicote à carne não reduziria preço e só afetaria açougue, dizem analistas...', 'Após aumento no preço, grupos pedem boicote à compra de carne bovina', 'Em resposta à disparada nos preços da carne bovina no país, internautas começaram a compartilhar a ideia de um boicote ao produto. Segundo postagens nas redes sociais, como a carne de vaca é um alimento perecível, se os consumidores pararem de comprá-la e optarem por frango ou porco, os açougues não terão outra opção que não seja baixar o preço. Mas não funciona bem assim. De acordo com economistas ouvidos pelo UOL, uma tentativa de boicote só teria efeitos pontuais, não conseguiria reduzir o preço geral e prejudicaria apenas quem está na ponta da cadeia produtiva, os açougues e supermercados.... ', '5d188b044333b4c62676e0d4f53dade7.jpg'),
(4, '2019-12-05 19:58:46', '', '', 'Boicote à carne não reduziria preço e só afetaria açougue, dizem analistas...', 'Após aumento no preço, grupos pedem boicote à compra de carne bovina', 'Em resposta à disparada nos preços da carne bovina no país, internautas começaram a compartilhar a ideia de um boicote ao produto. Segundo postagens nas redes sociais, como a carne de vaca é um alimento perecível, se os consumidores pararem de comprá-la e optarem por frango ou porco, os açougues não terão outra opção que não seja baixar o preço. Mas não funciona bem assim. De acordo com economistas ouvidos pelo UOL, uma tentativa de boicote só teria efeitos pontuais, não conseguiria reduzir o preço geral e prejudicaria apenas quem está na ponta da cadeia produtiva, os açougues e supermercados.... ', 'cba2a355dc11678a805de3839a03e32d.jpg'),
(5, '2019-12-05 20:03:11', '', '', 'Boicote à carne não reduziria preço e só afetaria açougue, dizem analistas...', 'Após aumento no preço, grupos pedem boicote à compra de carne bovina', 'Em resposta à disparada nos preços da carne bovina no país, internautas começaram a compartilhar a ideia de um boicote ao produto. Segundo postagens nas redes sociais, como a carne de vaca é um alimento perecível, se os consumidores pararem de comprá-la e optarem por frango ou porco, os açougues não terão outra opção que não seja baixar o preço. Mas não funciona bem assim. De acordo com economistas ouvidos pelo UOL, uma tentativa de boicote só teria efeitos pontuais, não conseguiria reduzir o preço geral e prejudicaria apenas quem está na ponta da cadeia produtiva, os açougues e supermercados.... ', 'imagens/'),
(6, '2019-12-05 20:04:48', '', '', 'Boicote à carne não reduziria preço e só afetaria açougue, dizem analistas...', 'Após aumento no preço, grupos pedem boicote à compra de carne bovina', 'Em resposta à disparada nos preços da carne bovina no país, internautas começaram a compartilhar a ideia de um boicote ao produto. Segundo postagens nas redes sociais, como a carne de vaca é um alimento perecível, se os consumidores pararem de comprá-la e optarem por frango ou porco, os açougues não terão outra opção que não seja baixar o preço. Mas não funciona bem assim. De acordo com economistas ouvidos pelo UOL, uma tentativa de boicote só teria efeitos pontuais, não conseguiria reduzir o preço geral e prejudicaria apenas quem está na ponta da cadeia produtiva, os açougues e supermercados.... ', 'imagens/b7c0c7d6ce233d0fe18625ea3cd5bdb1.jpg'),
(7, '2019-12-05 20:09:24', '', '', 'Boicote à carne não reduziria preço e só afetaria açougue, dizem analistas...', 'Após aumento no preço, grupos pedem boicote à compra de carne bovina', 'Em resposta à disparada nos preços da carne bovina no país, internautas começaram a compartilhar a ideia de um boicote ao produto. Segundo postagens nas redes sociais, como a carne de vaca é um alimento perecível, se os consumidores pararem de comprá-la e optarem por frango ou porco, os açougues não terão outra opção que não seja baixar o preço. Mas não funciona bem assim. De acordo com economistas ouvidos pelo UOL, uma tentativa de boicote só teria efeitos pontuais, não conseguiria reduzir o preço geral e prejudicaria apenas quem está na ponta da cadeia produtiva, os açougues e supermercados.... ', ''),
(10, '2019-12-05 20:19:04', '', '', 'Boicote à carne não reduziria preço e só afetaria açougue, dizem analistas...', 'Após aumento no preço, grupos pedem boicote à compra de carne bovinaawaw', 'Em resposta à disparada nos preços da carne bovina no país, internautas começaram a compartilhar a ideia de um boicote ao produto. Segundo postagens nas redes sociais, como a carne de vaca é um alimento perecível, se os consumidores pararem de comprá-la e optarem por frango ou porco, os açougues não terão outra opção que não seja baixar o preço. Mas não funciona bem assim. De acordo com economistas ouvidos pelo UOL, uma tentativa de boicote só teria efeitos pontuais, não conseguiria reduzir o preço geral e prejudicaria apenas quem está na ponta da cadeia produtiva, os açougues e supermercados.... ', ''),
(11, '2019-12-05 20:24:42', '', '', 'adawawdwd', 'dawdawdawdadwawda', 'adwdawdawdawdwdawdaw', ''),
(12, '2019-12-05 20:24:44', '', '', 'adawawdwd', 'dawdawdawdadwawda', 'adwdawdawdawdwdawdaw', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(50) NOT NULL AUTO_INCREMENT,
  `data_cadastro` datetime NOT NULL,
  `nome_usuario` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `login` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `senha` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `data_cadastro`, `nome_usuario`, `login`, `email`, `senha`) VALUES
(7, '2019-12-05 05:27:34', 'Leonardo Rocha', 'Leonardo.Oliveira92', 'Leonardo.Oliveira80', 'teste'),
(9, '2019-12-05 05:32:44', 'Bruno Queiróz', 'Brunao.Queiróz', 'Brunao@senac.br', 'teste'),
(8, '2019-12-05 05:31:28', 'Guilherme Dezena', 'Guilherme.Dezena', 'Dezena@senac.br', 'teste'),
(10, '2019-12-05 05:34:44', 'Adriano Aclina', 'Adriano.aclina', 'Adriano@senac.br', 'teste');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
