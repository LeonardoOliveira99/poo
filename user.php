
<!doctype html>
<html lang="PT-BR">
<meta charset=utf-8>
  <head>

   <?php include("includes/head.php");?>
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  
  <style>
body {font-family: Arial;}

.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

.tab button:hover {
  background-color: #ddd;
}

.tab button.active {
  background-color: #ccc;
}

.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>
  </head>
  <body>
    
  <?php include("includes/menu.php");?>

<main role="main" class="container">

    <div class="container">
    <div class="tab">
  <button class="tablinks" id="TABalterardados" onclick="openCity(event, 'London')">Alterar dados</button>
  <button class="tablinks" onclick="openCity(event, 'Paris')">Gerenciar Notícias</button>
  <button class="tablinks" id="TABNoticiasAtivas" onclick="openCity(event, 'Tokyo')">Noticias Ativas</button>
</div>

<div id="London" class="tabcontent">

    <div class="card-body" id="meuFormUser">
            <h2 class="card-title text-center">Altere seus dados</h2>

            <form class="form-signin" method="post" >
            <div class="form-label-group">
              <label for="inputEmail">ID</label>
                <input name="id_usuario" type="text" id="id_usuario" class="form-control" disabled="disabled" required autofocus> 
              </div>
            <div class="form-label-group">
              <label for="inputEmail">Nome de usuário</label>
                <input name="nome_usuario"type="text" id="nome_usuario" class="form-control" placeholder="EX: Nome e Sobrenome" required autofocus> 
              </div>
            
            <div class="form-label-group">
              <label for="inputEmail">Login</label>
                <input name="login"type="text" id="login" class="form-control" placeholder="EX: nome.sobrenome" required autofocus> 
              </div>    
            
            <div class="form-label-group">
                  <label for="inputEmail">Email</label>
                  <input name="email"type="text" id="email" class="form-control" placeholder="EX: usuario@senac.br" required autofocus> 
              </div>
              
              <div class="form-label-group">
                <label for="inputPassword">Senha</label>
                <input name="senha" type="password" id="senha" class="form-control" placeholder="Digite a senha" required>
              </div>
              <h3 id="mensagemDados"></h3>
              <div class="custom-control custom-checkbox mb-3">
              </div>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" name="alterar" id="BTNalterar" value="alterarDados">Alterar</strong></button>
              <hr class="my-4">
            </form>
          </div>

</div>

<div id="Paris" class="tabcontent">
<div class="card-body" id="meuFormUser">
            Pesquisar Notícia por ID:
            <div style="display: flex;"><input type="text" id="PesquisaNoticia"> <button style="Width: 100px; height: 30px;" type="submit" id="BTNPesquisaNoticia">Pesquisar</button></div>
            <div id="id_noticia_none" style="display: none"></div>
            <h2 class="card-title text-center">Gerencie suas notícias</h2>

            <form class="form-signin" method="post" action="Alterarjson.php" >
              <label for="inputEmail">Titulo Principal</label>
            <div class="form-label-group" style="display: flex;">
              <input name="id_usuario" type="text" id="titulo_principal" class="form-control" required autofocus> Tipo: 
                <p><select class="basic" id="selectTipo" name="tipo"><option ></option><option id="Politica" value="Politica" >Politica</option><option id="Economia" value="Economia" >Economia</option><option id="Mundo" value="Mundo" >Mundo</option></select></p>
                Lado: 
                <p><select class="basic" id="selectLado"><option  name="lado"></option><option value="Esquerdo" id="optEsquerdo" >Esquerdo</option><option value="Direito" id="optDireito">Direito</option></select></p>
  
              </div>
            <div class="form-label-group">
                <label for="inputEmail">Resumo Principal</label>
                <input name="resumo_principal" type="text" id="resumo_principal" class="form-control"  required autofocus> 
              <label for="inputEmail">Texto da Notícia</label>
                <input name="texto_noticia"type="text" id="texto_noticia" class="form-control" required autofocus> 
                        
                  <label for="inputEmail">Imagem da Notícia</label>
                  <p><select class="basic" id="selectimg"><option  name="lado"></option><option value="politica.jpg" id="optEsquerdo" >Imagem Notícia Política</option><option value="mundo.png" id="optDireito">Imagem Notícia Mundo</option><option value="mundo.png" id="optDireito">Imagem Notícia Economia</option></select></p>
             
              <h3 id="mensagemNoticia"></h3>
              <div class="custom-control custom-checkbox mb-3">
              </div>
              <div id="Botoes">
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" name="NovaNoticia" id="BTNNovaNoticia" value="cadastrar">Criar Notícia</strong></button>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" name="alterar" id="BTNAlterarNoticia" value="alterar">Alterar Notícia</strong></button>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" name="Excluir" id="BTNExcluirNoticia" value="excluir">Excluir Notícia</strong></button>
              </div>
              <hr class="my-4">
            </form>
          </div>
</div>
</div>

<div id="Tokyo" class="tabcontent">
<div class="table-wrapper">
    <table class="fl-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Título Principal</th>
            <th>Resumo Principal</th>
            <th>Texto Notícia</th>
            <th>Header 5</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Content 1</td>
            <td>Content 1</td>
            <td>Content 1</td>
            <td>Content 1</td>
            <td style="display: flex; align-items: center !important;"><button class="btn btn-small btn-primary btn-block text-uppercase" type="submit" name="Excluir" id="BTNExcluirNoticia" value="excluir">Excluir Notícia</strong></button><button class="btn btn-small btn-primary btn-block text-uppercase" type="submit" name="Excluir" id="BTNExcluirNoticia" value="excluir">Excluir Notícia</strong></button></td>
        </tr>
        <tbody>
    </table>
</div>
</div>

<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>






    
   

</main>
<!-- Footer -->
<?php include ("footer.php")?>
<script>
$( "#Avalie" ).click(function() {
  $("#escondido").css("visibility","visible");
});</script>

<div id="escondido" class="escondido">
  <form  method="post" style="float: right;"> 
            <div class="form-label-group">
              <label style="color: white;">AVALIE-NOS</label>
              </div>
              <input type="text" name="" id="avaliacao">
              <button class="btn btn-primary btn-block text-uppercase" type="submit" name="EnviaAvaliacao" id="EnviaAvaliacao" value="EnviaAvaliacao">Enviar</button>
            </form>
</footer>
<?php include("meuJS.php");?>
<!-- Footer -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
<!-- <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <strong class="d-inline-block mb-2 text-primary">World</strong>
          <h3 class="mb-0">Featured post</h3>
          <div class="mb-1 text-muted">Nov 12</div>
          <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
          <a href="#" class="stretched-link">Continue reading</a>
        </div>
        <div class="col-auto d-none d-lg-block">
          <svg class="bd-placeholder-img" width="200" height="250" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>
        </div>
      </div>
    </div> -->