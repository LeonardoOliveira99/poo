
<!doctype html>
<html lang="PT-BR">
<meta charset=utf-8>
  <head>

   <?php include("includes/head.php");?>
   
   
  </head>
  <body>
    
  <?php include("includes/menu.php");?>

<main role="main" class="container">
  <div id="teste"></div>
    <div id="carouselExampleIndicators" class="carousel slide shadowCarousel" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      </ol>
       
      <div class="carousel-inner">
      <?php 
            $query = mysqli_query($con,"select * from noticias1"); 
            $cont=0;
            // $result = mysqli_stmt_get_result($query);
            // $teste = mysqli_fetch_assoc($query);
            // var_dump($teste);
            if ($dados = mysqli_fetch_assoc($query)){
          ?>
        <div class="carousel-item active">
       
          <img class="d-block w-100" src="imagens/<?php echo $dados['imagem']?>" alt="Segundo Slide">
            <div class="carousel-caption d-none d-md-block">
              <!-- <h5 id="meuH5" href="inicial.php">P</h5> -->
              <a href="noticia.php?id=<?=$dados['id_noticia']?>" id="tituloSlide"><?php echo $dados['titulo_principal']?></a>
              <p id="resumoSlide"><?php echo $dados['resumo_principal']?></p>
            </div>
        </div>
      <?php };?>
      </div>
      
    </div>
            
  <div class="row mb-2" id="divNoticias">
    <div class="col-md-12">
      <?php 
        $query = mysqli_query($con,"select * from noticias1"); 
        $cont=0;
        // $result = mysqli_stmt_get_result($query);
        // $teste = mysqli_fetch_assoc($query);
        // var_dump($teste);
        while ($dados = mysqli_fetch_assoc($query)){
          if($dados['lado'] == "esquerdo"){
      ?>
      
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative Esquerdo <?php echo $dados['tipo']?>">
        <div class="col-auto d-none d-lg-block">
          <img class="bd-placeholder-img" width="200" height="250" src="imagens/<?php echo $dados['imagem']?>" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title>
        </div>
        <div class="col p-4 d-flex flex-column position-static">
          <strong class="d-inline-block mb-2" id="<?php echo $dados['tipo'];?>"><?php echo $dados['tipo'];?></strong>
          <h3 class="mb-0"><?php echo $dados['titulo_principal'];?></h3>
          <div class="mb-1 text-muted"><?php echo $dados['data_noticia'];?></div>
          <p class="card-text mb-auto"><?php echo $dados['resumo_noticia'];?></p>
          <a href="noticia.php?id_noticia=<?=$id?>" class="stretched-link">Continue lendo...</a>
        </div>
      </div> 
      <?php } else {?>
        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative Direito">
        <div class="col p-4 d-flex flex-column position-static">
          <strong class="d-inline-block mb-2" id="<?php echo $dados['tipo'];?>"><?php echo $dados['tipo'];?></strong>
          <h3 class="mb-0"><?php echo $dados['titulo_principal'];?></h3>
          <div class="mb-1 text-muted"><?=$dados['resumo_principal'];?></div>
          <p class="card-text mb-auto"><?php echo $dados['texto_noticia'];?>...</p>
          <a href="noticia.php?id=<?=$dados['id_noticia']?>" class="stretched-link">Continue lendo...</a>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img class="bd-placeholder-img" width="200" height="250" src="imagens/<?php echo $dados['imagem'];?>" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title> 
        </div>
    </div>
      <?php };};?>
  </div>

</main>
<!-- Footer -->
<?php include("footer.php")?>
<!-- Footer -->
<!-- /.container -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script></body>
</html>
