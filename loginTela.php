<!doctype html>
<html lang="pt-br">
  <head>

  <link rel="stylesheet" type="text/css" href="meuestilo1.css" media="screen" />
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Login</title>
  
  </head>
  <body id="meuBody" class="skew">

  <div class="container" id="meuContainerLogin">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">

          <div class="card-body" id="meuFormLogin" id="p-skew">
            <h5 class="card-title text-center">Acesse sua conta</h5>
            <form class="form-signin" method="post" action="loginsession.php">
              <div class="form-label-group">
              <label for="inputEmail"><strong>Login<strong></label>
                <input name="login"type="text" id="login" class="form-control" placeholder="Digite seu Nome de usuário" required autofocus> 
              </div>

              <div class="form-label-group">
                <label for="inputPassword">Senha</label>
                <input name="senha" type="password" id="senha" class="form-control" placeholder="Digite a senha" required>
              </div>

              <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input" id="customCheck1">
                <label class="custom-control-label" for="customCheck1">Lembrar minha senha</label>
              </div>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" name="entrar">Entrar</button>
              <hr class="my-4">
            </form>
          </div>

          <div class="card-body" id="meuFormCadastro">
            <h5 class="card-title text-center">Ou Cadastre-se</h5>


            <form class="form-signin" method="post" action="loginsession.php" >
            <div class="form-label-group">
              <label for="inputEmail">Nome</label>
                <input name="nome_usuario"type="text" id="nome_usuario" class="form-control" placeholder="Nome e Sobrenome" required autofocus> 
              </div>
            
            <div class="form-label-group">
              <label for="inputEmail">Login</label>
                <input name="login"type="text" id="login" class="form-control" placeholder="nome.sobrenome" required autofocus> 
              </div>    
            
            <div class="form-label-group">
                  <label for="inputEmail">Email</label>
                  <input name="email"type="text" id="email" class="form-control" placeholder="usuario@senac.br" required autofocus> 
              </div>
              
              <div class="form-label-group">
                <label for="inputPassword">Senha</label>
                <input name="senha" type="password" id="senha" class="form-control" placeholder="Digite a senha" required>
              </div>

              <div class="custom-control custom-checkbox mb-3">
              </div>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" name="cadastrar">Cadastrar-se</strong></button>
              <hr class="my-4">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
  <!-- <div>
  <form method="post" action="login.php" id="formlogin">
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input class="form-control" id="login" name="login">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="senha" name="senha">
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
  <button type="submit" class="btn btn-primary">Cadastrar-se</button>
</form>
</div> -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>