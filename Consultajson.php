<?php

include('conexao.php');

/* HEADERS DO CORS */ 
header("Access-Control-Allow-Origin: *"); /* nao convem liberar todos os dominios */
header ("Access-Control-Allow-Methods: OPTIONS, HEAD, GET, POST");
header ("Access-Control-Allow-Headers: *");

/* obtendo o conteudo do body */
$json =  file_get_contents('php://input');

/* convertendo o body em formato json em objeto php*/
$Objjson = json_decode ($json);

// $id = $Objjson->id_usuario;
// $login = $pessoa->login;
// $nome_usuario = $pessoa->nome_usuario;
// $email = $pessoa->email;
// $senha = $pessoa->senha;
// $data = date("Y-m-d H:i:s");
// $pessoa->data = $data;

// if (isset($pessoa->id)) {
//     $preparada = mysqli_prepare($con, 'select * from usuarios where id_usuario = ?');
//     mysqli_stmt_bind_param($preparada,'i', $id);
//     if(mysqli_stmt_execute($preparada)){
//         echo $pessoa->nome = $login;
//     }else{
//        echo "0";
//     }
        
// }



if (isset($Objjson->id_usuario)) {
    $preparada = mysqli_prepare($con, 'select id_usuario, data_cadastro, nome_usuario, login, email, senha from usuarios where id_usuario = ?');

    mysqli_stmt_bind_param($preparada,'i', $Objjson->id_usuario);

    mysqli_stmt_execute($preparada);

    $result = mysqli_stmt_get_result($preparada);

    

    if($dados = $result->fetch_assoc()){
        $array = [$dados['id_usuario'] , $dados['data_cadastro'] , $dados['nome_usuario'] , $dados['login'] , $dados['email'] , $dados['senha']];
        echo json_encode($array);

    }else{
       echo "0";
    }
        
}



if (isset($Objjson->id_noticia)) {
    $preparada = mysqli_prepare($con, 'select * from noticias1 where id_noticia = ?');

    mysqli_stmt_bind_param($preparada,'i', $Objjson->id_noticia);

    mysqli_stmt_execute($preparada);

    $result = mysqli_stmt_get_result($preparada);

    

    if($dados = $result->fetch_assoc()){
        $array = [ $dados['titulo_principal'], $dados['resumo_principal'], $dados['texto_noticia'], $dados['tipo'], $dados['lado'], $dados['id_noticia']];
        echo json_encode($array, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        

    }else{
       echo "0";
    }
        
}

if(isset($Objjson->texto)){
    echo "certo";
}else{
    echo "erro";
}

?>

