<!-- <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top"> -->
<nav class="navbar navbar-expand-md fixed-top" id="meuMenu">
  <!-- <a class="navbar-brand" href="#">Navbar</a> -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="inicial.php" id="meuA">Início <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="noticia.php?id=0" id="meuA">Notícia</a>
      </li>
      
      <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li> -->
    </ul>
    
    <div class="col-4 d-flex justify-content-end align-items-center">
      <?php if(isset($logado)){?>
        <p id="meuAMenu"><?php echo"Bem vindo $logado";?></p>
        <button class="btn btn-sm btn-outline-secondary" id="meuBtnMyconta" onclick="window.location.href='user.php'">Minha Conta</button>
        <button class="btn btn-sm btn-outline-secondary" id="meuBtnSair"  onclick="window.location.href='session.php?sair=sair'">Sair</button>
        <?php }else{?>
        <a class="btn btn-sm btn-outline-secondary" id="meuA" href="loginTela.php">Login</a>
        <a class="btn btn-sm btn-outline-secondary" id="meuA" href="loginTela.php">Cadastre-se</a>
      <?php };?></p>
        
    </div>
    </div>

  </div>
</nav>